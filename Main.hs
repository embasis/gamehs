{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns #-}

module Main where

import Prelude hiding(Left,Right)
import Control.Monad
import Control.Monad.State
import Data.Maybe(fromMaybe)
import System.Random
import System.Exit
import qualified SDL.Init as SDLInit
import SDL.Video
import SDL.Event
import SDL.Input.Keyboard
import SDL.Time
import Linear.V2
import Linear.V4
import Linear.Affine
import Linear.Vector
import Snake
import qualified Graphics as G

w_max = 128
h_max = 128

data MainState = Ingame (Game,GameState) Float Direction | Lost

frame :: Window -> G.GState -> StateT MainState IO ()
frame wnd gst = do st <- get
                   newSt <- lift $ do sMsec <- ticks
                                      let startTime = (fromIntegral sMsec) / 1000
                                      rnum <- randomIO
                                      mdir <- execStateT (mapEvents handleEvent) Nothing
                                      let newSt = transState startTime rnum mdir st
                                      case newSt of Ingame (g,gs) gT _ -> do let [tileTex,foodTex] = G.textures gst
                                                                                 V2 fx fy = food gs
                                                                             G.setCoordinates (mapWidth g) (mapHeight g) gst
                                                                             G.renderQuads [V2 (fromIntegral fx) (fromIntegral fy)] foodTex
                                                                             G.renderQuads (interp gs startTime gT) tileTex
                                                                             G.swapBuffers wnd
                                                    Lost -> return ()
                                      return newSt
                   put newSt
               
transState :: Float -> Int -> Maybe Direction -> MainState -> MainState
transState sT r mdir st = case st of Lost -> Ingame ggs sT Right
                                     Ingame (g,gs) gT cdir -> if lost gs
                                                              then Lost
                                                              else if gT + (fromIntegral $ number gs)*(speed gs) < sT
                                                                   then Ingame (g,step cdir' r g gs) gT cdir'
                                                                   else Ingame (g,gs) gT cdir'
                                                           where cdir' = fromMaybe cdir (mdir >>= isInv)
                                                                 isInv d | d == invDirection (direction gs) = Nothing
                                                                         | otherwise                        = Just d
                        where ggs = newGame gw gh (intToTile gw gh ((2+) $ mod r (gw*gh-1))) 0.15
                              gw = 40
                              gh = 30

interp :: GameState -> Float -> Float -> [V2 Float]
interp gs cT gT = zipWith (^+^) offs . map (fmap fromIntegral) $ (snakeTiles sn)
                where sn = snake gs
                      move d = ((cT - sT) / (speed gs)) *^ (fromDirection d)
                      sT = gT + (fromIntegral $ (number gs) - 1)*(speed gs)
                      offs = (move (direction gs):) . map (move . invDirection) . snakeDirections $ sn

handleEvent :: Event -> StateT (Maybe Direction) IO ()
handleEvent (Event ts p) = case p of KeyboardEvent e -> handleKeyboard e
                                     _ -> return ()

handleKeyboard :: KeyboardEventData -> StateT (Maybe Direction) IO ()
handleKeyboard (KeyboardEventData _ m _ (Keysym sc _ _)) = case m of Pressed -> key
                                                                     _ -> return ()
                                                         where key | sc == ScancodeEscape = lift exitSuccess
                                                                   | sc == ScancodeW = put $ Just Up
                                                                   | sc == ScancodeS = put $ Just Down
                                                                   | sc == ScancodeA = put $ Just Left
                                                                   | sc == ScancodeD = put $ Just Right
                                                                   | otherwise = return ()

main :: IO ()
main = do let wndCfg = WindowConfig
                { windowBorder       = False
                , windowHighDPI      = False
                , windowInputGrabbed = False
                , windowMode         = Windowed
                , windowOpenGL       = Just glCfg
                , windowPosition     = Absolute (P (V2 0 0))
                , windowResizable    = False
                , windowInitialSize  = V2 800 600
                }
              glCfg = OpenGLConfig
                { glColorPrecision = V4 8 8 8 0
                , glDepthPrecision = 24
                , glStencilPrecision = 8
                , glProfile = Core Normal 3 3
                }
          SDLInit.initializeAll
          window <- createWindow "Snake" wndCfg
          showWindow window
          glc <- glCreateContext window
          glMakeCurrent window glc
          gst <- G.setup (w_max*h_max)
          flip evalStateT Lost . forever $ frame window gst