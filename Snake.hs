module Snake( Game,
              mapWidth,
              mapHeight,
              GameState,
              snake,
              direction,
              food,
              speed,
              lost,
              number,
              Tile,
              Direction(..),
              Snake,
              snakeTiles,
              snakeHead,
              snakeDirections,
              newGame,
              step,
              fromDirection,
              invDirection,
              intToTile ) where

import Prelude hiding(Left,Right)
import Control.Applicative((<*>))
import Data.Maybe(catMaybes,fromMaybe)
import Linear.V2
import Linear.Vector

data Game = Game
    { mapWidth :: Int
    , mapHeight :: Int
    }
data GameState = GameState
    { snake :: Snake
    , direction :: Direction
    , food :: Tile
    , speed :: Float
    , lost :: Bool
    , number :: Int
    }
type Tile = V2 Int
data Direction = Up | Down | Left | Right
               deriving(Eq)
data Snake = Snake
    { snakeHead :: Tile
    , snakeDirections :: [Direction]
    }

startSnake = Snake (V2 1 1) []

newGame :: Int -> Int -> Tile -> Float -> (Game,GameState)
newGame w h f s = (Game w h,GameState startSnake Right f s False 1)

step :: Direction -> Int -> Game -> GameState -> GameState
step d r g oldState@(GameState sn dr f sp l num) | l = oldState
                                                 | otherwise = newState
                                                 where newState = GameState
                                                        { snake = nsn
                                                        , direction = d
                                                        , food = if nh == f then fromMaybe f mf else f
                                                        , speed = sp
                                                        , lost = leftBounds g (snake newState) || (mf == Nothing) || intersect
                                                        , number = num + 1
                                                        }
                                                       Snake h dirs = sn
                                                       nsn | nh == f = Snake f ((invDirection dr):dirs)
                                                           | otherwise = snakeStep dr sn
                                                       nh = tileStep dr h
                                                       mf = makeFood r g sn f
                                                       intersect = elem nh $ snakeTiles sn

fromDirection :: (Num a) => Direction -> V2 a
fromDirection d = case d of Up -> V2 0 1
                            Down -> V2 0 (-1)
                            Left -> V2 (-1) 0
                            Right -> V2 1 0
               
invDirection :: Direction -> Direction
invDirection d = case d of Up -> Down
                           Down -> Up
                           Left -> Right
                           Right -> Left

tileStep :: Direction -> Tile -> Tile
tileStep d t = t ^+^ (fromDirection d)

snakeTiles :: Snake -> [Tile]
snakeTiles (Snake h d) = scanl (^+^) h (fromDirection <$> d)

freeTiles :: Game -> Snake -> Tile -> [Tile]
freeTiles (Game w h) s f = catMaybes $ inc <$> [1..w] <*> [1..h] 
                         where st = snakeTiles s
                               inc x y | elem v st || (f == v) = Nothing
                                       | otherwise = Just v
                                       where v = V2 x y

snakeStep :: Direction -> Snake -> Snake
snakeStep d (Snake h dl) = Snake (tileStep d h) init'
                         where init' = case dl of [] -> []
                                                  _ -> (invDirection d):(init dl)

makeFood :: Int -> Game -> Snake -> Tile -> Maybe Tile
makeFood r g s f = case ft of [] -> Nothing
                              _ -> Just $ ft!!(mod r (length ft))
                 where ft = freeTiles g s f

-- left to right, bottom to top
intToTile :: Int -> Int -> Int -> Tile
intToTile w h n = V2 x y
                where x = n - (y-1)*w
                      y = 1 + (div (n-1) w)

leftBounds :: Game -> Snake -> Bool
leftBounds (Game w h) (Snake (V2 x y) _) = x > w || x < 1 || y > h || y < 1