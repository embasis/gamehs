{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns #-}

module Graphics( GState(..),
                 setup,
                 renderQuads,
                 setCoordinates,
                 swapBuffers ) where

import Control.Monad(mapM_,sequence_)
import Control.Applicative((<*>))
import System.Exit(exitSuccess)
import SDL.Video(showSimpleMessageBox,glSwapWindow,MessageKind(Error),Window)
import qualified Data.Text as T
import qualified Data.Vector.Storable as V
import Linear.V2
import Linear.Vector
import Codec.Picture
import Graphics.GL.Core32
import Foreign.C.String
import Foreign.C.Types
import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal.Array
import Foreign.Marshal.Alloc
import Debug.Trace(trace)

data GState = GState
    { vbo :: GLuint
    , gbuf :: GLuint
    , shader :: GLuint
    , textures :: [GLuint]
    , maxTiles :: Int
    }

vertexShader = concat [
    "#version 330 core\n",
    "uniform mat4 matrix;\n",
    "layout( location = 0 ) in vec4 inpos;",
    "layout( location = 1 ) in vec2 inuv;",
    "out vec2 uv;",
    "void main(){",
    "gl_Position = matrix * inpos;",
    "uv = inuv;",
    "}"
 ]

pixelShader = concat [
    "#version 330 core\n",
    "uniform sampler2D diffuseMap;",
    "in vec2 uv;",
    "out vec4 oclr;",
    "void main( ){",
    "oclr = texture( diffuseMap, uv );",
    "}"
 ]

projMat l r b t n f = [ 2 / rml, 0, 0, 0,
                        0, 2 / tmb, 0, 0,
                        0, 0, 2 / fmn, 0,
                        -rpl / rml, -tpb / tmb, -fpn / fmn, 1 ]
                    where rml = r-l
                          tmb = t-b
                          fmn = f-n
                          rpl = r+l
                          tpb = t+b
                          fpn = f+n

setup :: Int -> IO GState
setup maxt = do glClearColor 1 1 1 0
                shader' <- setupShaders
                (vbo',gbuf') <- setupBuffers (fromIntegral maxt)
                textures' <- setupTextures shader'
                glEnable GL_BLEND
                glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
                return $ GState vbo' gbuf' shader' textures' maxt

renderQuads :: [V2 Float] -> GLuint -> IO ()
renderQuads qs tex = withArray coordl draw
                   where coordl = concat $ map coords qs
                         coords q = verts . zip [(1,1),(0,1),(0,0),(1,0)] . map (q^+^) $ [V2 0 0,V2 (-1) 0,V2 (-1) (-1),V2 0 (-1)]
                         verts [a,b,c,d] = concat $ (tr [a,b,d]) ++ (tr [b,c,d])
                         tr = map (\((u,v),V2 x y) -> [x,y,u,v])
                         draw pData = do glBufferSubData GL_ARRAY_BUFFER 0 size pData
                                         glBindTexture GL_TEXTURE_2D tex
                                         glDrawArrays GL_TRIANGLES 0 (fromIntegral . (*6) $ length qs)
                         size = fromIntegral $ (length coordl) * (sizeOf (undefined :: CFloat))

setCoordinates :: Int -> Int -> GState -> IO ()
setCoordinates w h gst = do trMatLoc <- withCString "matrix" (glGetUniformLocation (shader gst))
                            withArray (projMat 0 (fromIntegral w) 0 (fromIntegral h) (-1) 1) (glUniformMatrix4fv trMatLoc 1 GL_FALSE)                       

setupShaders :: IO GLuint
setupShaders = do vsh <- loadShader GL_VERTEX_SHADER vertexShader
                  psh <- loadShader GL_FRAGMENT_SHADER pixelShader
                  shpr <- glCreateProgram
                  mapM_ (glAttachShader shpr) [vsh,psh]
                  glLinkProgram shpr
                  shaderCheckStage shpr GL_LINK_STATUS glGetProgramiv glGetProgramInfoLog
                  sequence_ $ [glDetachShader shpr,glDeleteShader] <*> [vsh,psh]
                  glUseProgram shpr
                  return shpr

setupTextures :: GLuint -> IO [GLuint]
setupTextures pr = mapM setupTexture ["tile.png","food.png"]
                 where setupTexture fname = do edi <- readImage fname
                                               glActiveTexture GL_TEXTURE0
                                               sampler <- withCString "diffuseMap" (glGetUniformLocation pr)
                                               glUniform1i sampler 0
                                               case edi of Left msg -> do showSimpleMessageBox Nothing Error "TEXTURE LOAD ERROR" (T.pack msg)
                                                                          exitSuccess
                                                           Right di -> V.unsafeWith d uploadTexture
                                                                    where (Image w h d) = pixelMap fixAlphas . convertRGBA8 $ di
                                                                          fixAlphas (PixelRGBA8 255 255 255 _) = PixelRGBA8 255 255 255 0
                                                                          fixAlphas p = p
                                                                          uploadTexture pImg = alloca create
                                                                                             where w' = fromIntegral w
                                                                                                   h' = fromIntegral h
                                                                                                   create pTex = do glGenTextures 1 pTex
                                                                                                                    tex <- peek pTex
                                                                                                                    glBindTexture GL_TEXTURE_2D tex
                                                                                                                    glTexImage2D GL_TEXTURE_2D 0 (fromIntegral GL_RGBA) w' h' 0 GL_RGBA GL_UNSIGNED_BYTE pImg
                                                                                                                    let texParam = flip (glTexParameteri GL_TEXTURE_2D)
                                                                                                                    mapM_ (texParam (fromIntegral GL_REPEAT)) [GL_TEXTURE_WRAP_S,GL_TEXTURE_WRAP_T]
                                                                                                                    mapM_ (texParam (fromIntegral GL_LINEAR)) [GL_TEXTURE_MIN_FILTER,GL_TEXTURE_MAG_FILTER]
                                                                                                                    return tex
                                                                                               

setupBuffers :: GLsizeiptr -> IO (GLuint,GLuint)
setupBuffers maxt = do pVbo <- malloc
                       glGenVertexArrays 1 pVbo
                       pBuf <- malloc
                       glGenBuffers 1 pBuf
                       vbo <- peek pVbo
                       buf <- peek pBuf                           
                       free pVbo
                       free pBuf
                       glBindVertexArray vbo
                       glBindBuffer GL_ARRAY_BUFFER buf
                       glBufferData GL_ARRAY_BUFFER (6 * floatSize * maxt * 4) nullPtr GL_STREAM_DRAW
                       let enableAttribs (a,off) = do glVertexAttribPointer a 2 GL_FLOAT GL_FALSE (4 * floatSize) (intPtrToPtr off)
                                                      glEnableVertexAttribArray a
                       mapM_ enableAttribs [(attribPos,0),(attribUv,2 * floatSize)]
                       return (vbo,buf)
                  where floatSize :: (Integral a) => a
                        floatSize = fromIntegral $ sizeOf (undefined :: CFloat)
                        attribPos = 0
                        attribUv = 1

loadShader :: GLenum -> String -> IO GLuint
loadShader ty te = do sh <- glCreateShader ty
                      let setShSrc src = alloca (\pSrc -> poke pSrc src >> glShaderSource sh 1 pSrc nullPtr)
                      withCString te setShSrc
                      glCompileShader sh
                      shaderCheckStage sh GL_COMPILE_STATUS glGetShaderiv glGetShaderInfoLog
                      return sh

shaderCheckStage obj stc stf msgf = alloca checkStatus
                                  where checkStatus pSt = do stf obj stc pSt
                                                             let checkFail st pMsg | st == 0   = do msgf obj pMsgLen nullPtr pMsg
                                                                                                    msg <- peekCString pMsg
                                                                                                    showSimpleMessageBox Nothing Error "SHADER COMPILE ERROR" (T.pack msg)
                                                                                                    exitSuccess
                                                                                   | otherwise = return ()
                                                             st <- peek pSt
                                                             allocaArray0 (fromIntegral pMsgLen) (checkFail st)
                                                        where pMsgLen = 4096

swapBuffers :: Window -> IO ()
swapBuffers wnd = do glSwapWindow wnd
                     glClear GL_COLOR_BUFFER_BIT